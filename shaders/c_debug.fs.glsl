varying vec2 v_texCoord;
varying float v_point_x;
varying float v_point_y;
uniform int t;
uniform int maxiters;
uniform float c_x;
uniform float c_y;
const float PI=3.1415926535897932384626433832795;

vec2 Prod(vec2 v1, vec2 v2)
{
	return vec2(v1.x*v2.x - v1.y*v2.y, v1.x*v2.y + v1.y*v2.x);
}

int fmandel(void)
{
	vec2 z = vec2(v_point_x, v_point_y);
	vec2 c = vec2(c_x, c_y);
	float radius = 30.0;
	//vec2 c = vec2(0.285, 0.01);

//	for (int n=0; n<maxiters; n++)
//	{
//		z = Prod(z, z) + c;
//		if(sqrt(z.x*z.x + z.y*z.y) > radius)
//			return n;
//	}
	for (int n=0; n<1; n++)
	{
		z = Prod(z, z) + c;
		if(sqrt(z.x*z.x + z.y*z.y) > radius)
			return n;
	}
	return maxiters;
}

void main()
{
	vec4 color;
	float velocidad_color = 0.5;
	int sale = fmandel();
	float sale_f = float(sale) / 30.0;
	vec2 c = vec2(c_x, c_y);    // test if 2fv works
	gl_FragColor = vec4(c[0], c[1], 0, 1);    // debug mouse movement
//	gl_FragColor = vec4((-cos(velocidad_color*float(sale))+1.0)/2.0,
//			(-cos(velocidad_color*float(sale)+ 2.0)+1.0)/2.0,
//			(-cos(velocidad_color*float(sale) + 4.0)+1.0)/2.0,
//			1.0);
}

