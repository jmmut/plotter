varying vec2 v_texCoord;
varying float v_point_x;
varying float v_point_y;
uniform int t;
uniform int maxiters;
uniform vec2 c;
uniform float c_x;
uniform float c_y;
const float PI=3.1415926535897932384626433832795;

vec2 Prod(vec2 v1, vec2 v2)
{
	return vec2(v1.x*v2.x - v1.y*v2.y, v1.x*v2.y + v1.y*v2.x);
}

int fmandel(void)
{
	vec2 z = vec2(v_point_x, v_point_y);
	vec2 c = vec2(c_x, c_y);
	float radius = 30.0;
	//vec2 c = vec2(0.285, 0.01);

//	for (int n=0; n<maxiters; n++)
//	{
//		z = Prod(z, z) + c;
//		if(sqrt(z.x*z.x + z.y*z.y) > radius)
//			return float(n)/float(maxiters);
//	}

    int maxiters_local = 31;
	for (int n = 0; n < maxiters_local; n++)
	{
		z = Prod(z, z) + c;
		if(sqrt(z.x*z.x + z.y*z.y) > radius)
		{
		    return n;
		}
	}
	return 1.0;
}

void main()
{
	vec4 color;
	float velocidad_color = 3.0;
	int sale = fmandel();
    float t_f = float(t) * 0.02;
//	float sale_f = float(sale);
//	vec2 c = vec2(c_x, c_y);    // test if 2fv works
	gl_FragColor = vec4(float(maxiters)/100.0,
	(cos(velocidad_color*sale_f + 0.0)+1.0)*0.5,
	0.0, 1.0);
//	gl_FragColor = vec4(
//	        (cos(velocidad_color*sale_f + 0.0 + t_f)+1.0)*0.5,
//			(cos(velocidad_color*sale_f + 1.0 + t_f)+1.0)*0.5,
//			(cos(velocidad_color*sale_f + 2.0 + t_f)+1.0)*0.5,
//			1.0);
}

