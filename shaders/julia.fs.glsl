varying vec2 v_texCoord;
varying float v_point_x;
varying float v_point_y;
uniform int t;
uniform int maxiters;
uniform vec2 c;
uniform float c_x;
uniform float c_y;
const float PI=3.1415926535897932384626433832795;

vec2 Prod(vec2 v1, vec2 v2)
{
	return vec2(v1.x*v2.x - v1.y*v2.y, v1.x*v2.y + v1.y*v2.x);  // complex product: (a+bi)*(c+di) = a*c - b*d + (a*d + b*c)*i
}

dvec2 Prod(dvec2 v1, dvec2 v2)
{
	return dvec2(v1.x*v2.x - v1.y*v2.y, v1.x*v2.y + v1.y*v2.x);  // complex product: (a+bi)*(c+di) = a*c - b*d + (a*d + b*c)*i
}
int dmandel(void)
{
	dvec2 z = vec2(v_point_x, v_point_y);
	dvec2 c = dvec2(double(c_x), c_y);  // comment/uncomment: test wether vec2 works or not
	// float radius = 30.0;
    double radius = 30.0;
	//vec2 c = vec2(0.285, 0.01);

	for (int n=0; n<maxiters; n++)
	{
		z = Prod(z, z) + c;
		if(sqrt(z.x*z.x + z.y*z.y) > radius)
			return n;
	}

	return maxiters;
}

/** this returns an int to make further iterations maintain the color of outer patters, 
 * if it returned a float [0, 1] the colors would stretch, making more difficult to discern borders
 */
int fmandel(void)
{
	vec2 z = vec2(v_point_x, v_point_y);
	vec2 c = vec2(c_x, c_y);  // comment/uncomment: test wether vec2 works or not
	float radius = 30.0;
	//vec2 c = vec2(0.285, 0.01);

	for (int n=0; n<maxiters; n++)
	{
		z = Prod(z, z) + c;
		if(sqrt(z.x*z.x + z.y*z.y) > radius)
			return n;
	}

	return maxiters;
}

void main()
{
	vec4 color;
	float velocidad_color = 0.1;
	int sale = fmandel();
    float t_f = float(t) * 0.02;
	float sale_f = float(sale);
//	vec2 c = vec2(c_x, c_y);    // test if 2fv works

	gl_FragColor = vec4(
	        (cos(velocidad_color*float(sale) + 0.0 /* + t_f */)+1.0)*0.5,
			(cos(velocidad_color*float(sale) + 2.0 /* + t_f */)+1.0)*0.5,
			(cos(velocidad_color*float(sale) + 4.0 /* + t_f */)+1.0)*0.5,
			1.0);
}

