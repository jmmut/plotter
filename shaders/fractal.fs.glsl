varying vec2 v_texCoord;
varying vec2 v_point;
uniform int t;
uniform int maxiters;
uniform vec2 c;
const double PI=3.1415926535897932384626433832795;

vec2 Prod(vec2 v1, vec2 v2)
{
	return vec2(v1.x*v2.x - v1.y*v2.y, v1.x*v2.y + v1.y*v2.x);
}

int fmandel(void)
{
	vec2 z = vec2(v_point);
	float radius = 3.0;
	//vec2 c = vec2(0.285, 0.01);
	vec2 aux;

	for (int n=0; n < maxiters; n++)
	{
		z = Prod(z,z) + c;
		if(sqrt(float(z.x*z.x + z.y*z.y)) > radius)
			return n;
	}
	return maxiters;
}

void main()
{
	vec4 color;
	float velocidad_color = 0.1;
	int sale = fmandel();
	
	gl_FragColor = vec4((-cos(velocidad_color*float(sale))+1.0)/2.0,
			(-cos(velocidad_color*float(sale)+ 2)+1.0)/2.0,
			(-cos(velocidad_color*float(sale) + 4)+1.0)/2.0,
			1.0);
}

