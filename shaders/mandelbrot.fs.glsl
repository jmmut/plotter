


varying vec2 v_texCoord;
varying vec2 v_point;
varying float v_point_x;
varying float v_point_y;
uniform int t;
uniform int maxiters;

vec2 Prod(vec2 v1, vec2 v2)
{
	return vec2(v1.x*v2.x - v1.y*v2.y, v1.x*v2.y + v1.y*v2.x);  // complex product: (a+bi)*(c+di) = a*c - b*d + (a*d + b*c)*i
}

int fmandel(void)
{
	vec2 z = vec2(v_point_x, v_point_y);
    vec2 v_point = vec2(v_point_x, v_point_y);
	float radius = 30.0;

	for (int n=0; n<maxiters; n++)
	{
		//z = Prod(z, Prod(z,z)) + v_point;
		z = Prod(z,z) + v_point;
		if((z.x*z.x + z.y*z.y) > radius)
			return n;
	}
	return maxiters;
}

void main()
{
	//int i;
	vec4 color;
	//float diversiones = 0.0;
	//float final = 0.0;
	//vec2 z;
	int sale = fmandel();

	//for(i = 0; i < maxiters; i++) { 
		//if (sqrt(z.x*z.x + z.y*z.y) > 1.0)
			////if (z.x > 1.0 && z.y > 1.0)
			////diversiones++;
			//aux = z.x*z.x - z.y*z.y + v_point.x; 
		//z.y = z.x*z.y + z.y*z.x + v_point.y; 
		//z.x = aux; 
	//}
//
	////if (sqrt(z.x*z.x + z.y*z.y) > 1.0)
	//if (z.x > 1.0 && z.y > 1.0)
		//final = 1.0;


	//color.r = 1-float(sale)/float(maxiters);
	//color.r = cos(0.025*float(sale));
	//color.g = sin(0.025*float(sale));
	//color.b = 0.0;
	//color.a = 1.0;
	//gl_FragColor = color;
	// gl_FragColor = vec4((-cos(0.1*float(sale))+1.0)/2.0,
	// 		(-cos(0.1*float(sale)+ 2)+1.0)/2.0,
	// 		(-cos(0.1*float(sale) + 4)+1.0)/2.0,
	// 		1.0);


	float velocidad_color = 0.1;
	gl_FragColor = vec4(
	        (cos(velocidad_color*float(sale) + 2.0 /* + t_f */)+1.0)*0.5,
			(cos(velocidad_color*float(sale) + 3.0 /* + t_f */)+1.0)*0.5,
			(cos(velocidad_color*float(sale) - 1.0 /* + t_f */)+1.0)*0.5,
			1.0);
}

