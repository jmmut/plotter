varying vec2 v_texCoord;
attribute float point_x;
attribute float point_y;
varying float v_point_x;
varying float v_point_y;

void main()
{
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	v_texCoord = vec2(gl_MultiTexCoord0);
	v_point_x = point_x;
	v_point_y = point_y;
}

