


varying vec2 v_texCoord;
varying vec2 v_point;
uniform int t;
uniform int maxiters;

vec2 Prod(vec2 v1, vec2 v2)
{
	return vec2(v1.x*v2.x - v1.y*v2.y, v1.x*v2.y + v1.y*v2.x);
}

int fmandel(void)
{
	vec2 z = v_point;
	float radius = 30.0;

	for (int n=0; n<maxiters; n++)
	{
		z = Prod(z, Prod(z,z)) + v_point;
		if((z.x*z.x + z.y*z.y) > radius)
			return n;
	}
	return maxiters;
}

void main()
{
	vec4 color;
	float grosor = 1.0;
	float x = v_point.x;
	float y = v_point.y;
	
	float vert_axis = 1 - 10*abs(x - round(x));
	float hori_axis = 1 - 10*abs(y - round(y));
	
	float valor = x*x;
	
	float dif = 1 - grosor*abs(valor - v_point.y);
	
	//gl_FragColor = vec4(cos(valor)
			//, cos(valor+ 3.1416)
			//, 0.0
			//, 1.0);
	gl_FragColor = vec4((dif)
			, vert_axis
			, hori_axis
			, 1.0);
	
}

