varying vec2 v_texCoord;
varying vec2 v_point;
uniform int t;
uniform int maxiters;

vec2 Prod(vec2 v1, vec2 v2)
{
	return vec2(v1.x*v2.x - v1.y*v2.y, v1.x*v2.y + v1.y*v2.x);
}

void main()
{
	vec4 color;
	float accuracy = 3.0;
	float x = v_point.x;
	float y = v_point.y;
	
	float vert_axis = 1 - 50*abs(x - round(x));
	float hori_axis = 1 - 50*abs(y - round(y));
	if (vert_axis < 0) vert_axis = 0;
	if (hori_axis < 0) hori_axis = 0;
	
	/* pon la funcion que quieras plotear */
	//float valor = sqrt(4*4-x*x);
	//float valor = sin(x);
	float valor = x*sin(x);
	
	float dif = 1 - accuracy*abs(valor - v_point.y);
	//float dif = 1 - accuracy*float(valor < v_point.y);

	//valor = x*x;
	//float dif2 = 1 - accuracy*abs(valor - v_point.y);
		
	if (dif < 0) dif = 0.0;
	//if (dif2 < 0) dif2 = 0.0;
	
	gl_FragColor = vec4((dif)
			, vert_axis + float(round(10*y) == 0.0 )
			, hori_axis + float(round(10*x) == 0.0 )
			, 1.0);
	
}

