//
// Created by jmmut on 2015-06-09.
//

#ifndef PLOTTER_MEMORIA_H
#define PLOTTER_MEMORIA_H


#include <SDL.h>
#include <complex>
#include <math/Vector3D.h>

typedef union
{
    Uint32 conjunto;
    struct
    {
        Uint8 b;	// c[0]
        Uint8 g;	// c[1] ...
        Uint8 r;
        Uint8 a;
    } componentes;
    Uint8 c[4];
} Color;


struct Memoria
{
    Color color_bg, color_diverge, color_converge;
    complex<double> inivent, tamvent, zona_local;
    int maxiters;
    Color paleta[6*255];
    complex<double> dir;
    int last_click_x, last_click_y;
    int t;
    complex<double> c;
    bool modo;
    int xant, yant;
    complex<double> pos;
};



#endif //PLOTTER_MEMORIA_H
