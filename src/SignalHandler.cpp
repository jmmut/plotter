
#include "SignalHandler.h"

#ifndef DO_NOT_BACKTRACE
using namespace std;
//bool SignalHandler::mbGotExitSignal = false;
//
///**
//* Default Contructor.
//*/
//SignalHandler::SignalHandler()
//{
//}
//
///**
//* Destructor.
//*/
//SignalHandler::~SignalHandler()
//{
//}
//
///**
//* Returns the bool flag indicating whether we received an exit signal
//* @return Flag indicating shutdown of program
//*/
//bool SignalHandler::gotExitSignal()
//{
//    return mbGotExitSignal;
//}
//
///**
//* Sets the bool flag indicating whether we received an exit signal
//*/
//void SignalHandler::setExitSignal(bool _bExitSignal)
//{
//    mbGotExitSignal = _bExitSignal;
//}
//
///**
//* Sets exit signal to true.
//* @param[in] _ignored Not used but required by function prototype
//*                     to match required handler.
//*/
//void SignalHandler::exitSignalHandler(int _ignored)
//{
//    mbGotExitSignal = true;
//    cout << "creando signal exception" << endl;
//    throw SignalException("mi excepcion de señal");
//}
//
///**
//* Set up the signal handlers for CTRL-C.
//*/
//void SignalHandler::setupSignalHandlers()
//{
//    if (signal((int) SIGSEGV, SignalHandler::exitSignalHandler) == SIG_ERR)
//    {
//        throw SignalException("!!!!! Error setting up signal handlers !!!!!");
//    }
//}
//

/** Print a demangled stack backtrace of the caller function to FILE* out.
 * Courtesy of (c) 2008, Timo Bingmann from http://idlebox.net/
 * published under the WTFPL v2.0
 */
void print_stacktrace(ostream &out, unsigned int max_frames)
{
    out << "stack trace:\n";
    size_t max_line_length = 2000;

    // storage array for stack trace address data
    void* addrlist[max_frames+1];

    // retrieve current stack addresses
    int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void*));

    if (addrlen == 0) {
        out << "  <empty, possibly corrupt>\n";
        return;
    }

    // resolve addresses into strings containing "filename(function+address)",
    // this array must be free()-ed
    char** symbollist = backtrace_symbols(addrlist, addrlen);

    // allocate string which will be filled with the demangled function name
    size_t funcnamesize = 256;
    char* funcname = (char*)malloc(funcnamesize);

    // iterate over the returned symbol lines. skip the first, it is the
    // address of this function.
    for (int i = 1; i < addrlen; i++)
    {
        char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

        // find parentheses and +address offset surrounding the mangled name:
        // ./module(function+0x15c) [0x8048a6d]
        for (char *p = symbollist[i]; *p; ++p)
        {
            if (*p == '(')
                begin_name = p;
            else if (*p == '+')
                begin_offset = p;
            else if (*p == ')' && begin_offset) {
                end_offset = p;
                break;
            }
        }

        if (begin_name && begin_offset && end_offset
            && begin_name < begin_offset)
        {
            *begin_name++ = '\0';
            *begin_offset++ = '\0';
            *end_offset = '\0';

            // mangled name is now in [begin_name, begin_offset) and caller
            // offset in [begin_offset, end_offset). now apply
            // __cxa_demangle():

            int status;
            char* ret = abi::__cxa_demangle(begin_name,
                    funcname, &funcnamesize, &status);
            char str[max_line_length];
            if (status == 0) {
                funcname = ret; // use possibly realloc()-ed string
                snprintf(str, max_line_length, "  %s : %s+%s\n",
                        symbollist[i], funcname, begin_offset);
            }
            else {
                // demangling failed. Output function name as a C function with
                // no arguments.
                snprintf(str, max_line_length, "  %s : %s()+%s\n",
                        symbollist[i], begin_name, begin_offset);
            }
            out << str;
        }
        else
        {
            // couldn't parse the line? print the whole line.
            out <<  symbollist[i] << endl;
        }
    }

    free(funcname);
    free(symbollist);
}




ostream * SigsegvPrinter::out(nullptr);

void SigsegvPrinter::activate(std::ostream &os) {
    out = &os;
    if (signal((int) SIGSEGV, handleSignal) == SIG_ERR) {
        throw SignalException("!!!!! Error setting up signal handlers !!!!!");
    }
}

void SigsegvPrinter::handleSignal(int) {
    string message = "exception wrapper for a segmentation fault. ";
    if (out == nullptr) {
        message += "'ostream' for the backtrace was not set, you can fix this writing 'SigsegvPrinter::activate(cout);' in your program. ";
    } else {
        print_stacktrace(*out);
    }
    
    throw SignalException(message);
}

#else // DO_NOT_BACKTRACE

void print_stacktrace(std::ostream &out, unsigned int max_frames) {}
void SigsegvPrinter::activate(std::ostream &os) {}
void SigsegvPrinter::handleSignal(int) {}

#endif // DO_NOT_BACKTRACE