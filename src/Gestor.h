//
// Created by jmmut on 2015-06-09.
//

#ifndef PLOTTER_GESTOR_H
#define PLOTTER_GESTOR_H


#include <vgm/Ventana.h>
#include <vgm/ShaderManager.h>
#include "Memoria.h"


class Gestor: public Ventana, public Memoria, public ShaderManager
{
public:
    Gestor(int width = 640, int height = 480);
    void initGL();
    void onStep(float);
    void onDisplay();
    void onKeyPressed(SDL_Event &e);
    void onMouseMotion(SDL_Event &e);
    
    string help();
};

#endif //PLOTTER_GESTOR_H
