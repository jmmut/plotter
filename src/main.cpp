/**
 * @file main.cpp
 */

#include "SignalHandler.h"
#include "Gestor.h"

using namespace std;

int main(int argc, char **argv)
{
    SigsegvPrinter::activate(cout);
    Gestor gestor(1300, 700);

    if (argc > 1) {
        cout << "this program does not take CLI parameters, only in runtime" << endl;
        cout << gestor.help();
    }
    gestor.mainLoop();

    cout << endl;
    return 0;
}
