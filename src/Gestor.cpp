//
// Created by jmmut on 2015-06-09.
//

#include "Gestor.h"

Gestor::Gestor(int width, int height): Ventana(width, height), ShaderManager()
{
    initGL();

    if (!initShaders())
        exit(1);
//    loadShaders("./jmmut/plotter/shaders/mandelbrot.vs.glsl", "./jmmut/plotter/shaders/julia.fs.glsl");
    loadShaders("./jmmut/plotter/shaders/mandelbrot.vs.glsl", "./jmmut/plotter/shaders/mandelbrot.fs.glsl");
    //loadShaders("./shaders/asdf.glsl", "./shaders/asdf.glsl");

    //currentShader = 2;
    t = 0;
    SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG);

    color_converge.conjunto = 0x0;
    color_diverge.conjunto = 0x00ffff;
    float ancho = 1.0;
    float alto = 1.0*height/width;
    inivent = complex<double>(-ancho, alto);
    tamvent = complex<double>(2*ancho, -2*alto);
    zona_local = complex<double>(2, 0);
    c = complex<double>(-0.8, 0.156);
    modo = false;
    xant = width/2;
    yant = height/2;

    maxiters = 20;
    setFps(60);
    semaforoStep.sumar();
    semaforoDisplay.sumar();
    int i;
    for (i = 0; i < 255; i++)
    {
        paleta[i].c[0] = 255;
        paleta[i].c[1] = i;
        paleta[i].c[2] = 0;
    }
    for (i = 0; i < 255; i++)
    {
        paleta[i+255].c[0] = 255 - i;
        paleta[i+255].c[1] = 255;
        paleta[i+255].c[2] = 0;
    }
    for (i = 0; i < 255; i++)
    {
        paleta[i+510].c[0] = 0;
        paleta[i+510].c[1] = 255;
        paleta[i+510].c[2] = i;
    }
    for (i = 0; i < 255; i++)
    {
        paleta[i+765].c[0] = 0;
        paleta[i+765].c[1] = 255 - i;
        paleta[i+765].c[2] = 255;
    }
    for (i = 0; i < 255; i++)
    {
        paleta[i+1020].c[0] = i;
        paleta[i+1020].c[1] = 0;
        paleta[i+1020].c[2] = 255;
    }
    for (i = 0; i < 255; i++)
    {
        paleta[i+1275].c[0] = 255;
        paleta[i+1275].c[1] = 0;
        paleta[i+1275].c[2] = 255-i;
    }	// el tope es 1530 = 255*6

    for(i = 0; i < 1530; i++)
        paleta[i].c[3] = 0;
}

/** A general OpenGL initialization function.
 * Sets all of the initial parameters.
 * We call this right after our OpenGL window is created.
 */
void Gestor::initGL()
{
    GLdouble aspect;
    int width = 640, height = 480;

    if (window == NULL)
        SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION
                , "There's no window, GL will use (640, 480) in the viewport, and you must create the sdl gl context ");
    else
    {
        SDL_GetWindowSize(window, &width, &height);
        context = SDL_GL_CreateContext(window);
        if (!context) {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unable to create OpenGL context: %s\n", SDL_GetError());
            SDL_Quit();
            exit(2);
        }
    }

    glViewport(0, 0, width, height);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);        /* This Will Clear The Background Color To Black */
    glClearDepth(1.0);                /* Enables Clearing Of The Depth Buffer */
    glDepthFunc(GL_LESS);                /* The Type Of Depth Test To Do */
    glEnable(GL_DEPTH_TEST);            /* Enables Depth Testing */
    glShadeModel(GL_SMOOTH);            /* Enables Smooth Color Shading */

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();                /* Reset The Projection Matrix */

    aspect = (GLdouble)width / height;
    glOrtho(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect, 0.0, 1.0);

    glMatrixMode(GL_MODELVIEW);
}

void Gestor::onStep(float dt)
{
    t++;
}

void Gestor::onDisplay()
{
    GLint location;
    GLfloat cpoint[2] = {(float)c.real(), (float)c.imag()};

    GLfloat point[2];
    int height, width;
    SDL_GetWindowSize(window, &width, &height);
    float ancho = 0.9;
    float alto = 0.9*height/width;

    char titulo[250];
    sprintf(titulo, "Shader Demo. iteraciones = %d, c = %f + i* %f", maxiters, cpoint[0], cpoint[1]);
    SDL_SetWindowTitle(window, titulo);


    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);        /* Clear The Screen And The Depth Buffer */

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    if (shadersSupported) {
        glUseProgramObjectARB(shaders[currentShader].program);
        location = glGetUniformLocationARB(shaders[currentShader].program, "t");
        if (location >= 0) {
            glUniform1iARB(location, t);
        }
        location = glGetUniformLocationARB(shaders[currentShader].program, "maxiters");
        if (location >= 0) {
            glUniform1iARB(location, maxiters);
        }
        location = glGetUniformLocationARB(shaders[currentShader].program, "c_x");
        if (location >= 0) {
            glUniform1fARB(location, cpoint[0]);
        }
        location = glGetUniformLocationARB(shaders[currentShader].program, "c_y");
        if (location >= 0) {
            glUniform1fARB(location, cpoint[1]);
        }
        location = glGetUniformLocationARB(shaders[currentShader].program, "c");
        if (location >= 0) {
            glUniform2fvARB(location, 2, cpoint);
        }
    }

    GLint point_x_location = glGetAttribLocationARB(shaders[currentShader].program, "point_x");
    GLint point_y_location = glGetAttribLocationARB(shaders[currentShader].program, "point_y");

    glLoadIdentity();                /* Reset The View */
    //glTranslatef(-1.5f,0.0f,0.0f);        /* Move Left 1.5 Units */

    // define a rectangle. if the shader works, it will write the texture.
    // otherwise, a simple color interpolation will indicate the error
    glBegin(GL_POLYGON);

    point[0] = inivent.real();
    point[1] = inivent.imag();
    glVertexAttrib1fARB(point_x_location, point[0]);
    glVertexAttrib1fARB(point_y_location, point[1]);
    glColor3f(1.0f, 0.0f,0.0f);
    glVertex3f(-ancho, alto, 0.0f);

    point[0] = inivent.real()+tamvent.real();
    point[1] = inivent.imag();
    glVertexAttrib1fARB(point_x_location, point[0]);
    glVertexAttrib1fARB(point_y_location, point[1]);
    glColor3f(0.0f,1.0f,0.0f);
    glVertex3f( ancho, alto, 0.0f);

    point[0] = inivent.real()+tamvent.real();
    point[1] = inivent.imag()+tamvent.imag();
    glVertexAttrib1fARB(point_x_location, point[0]);
    glVertexAttrib1fARB(point_y_location, point[1]);
    glColor3f(0.0f,0.0f,1.0f);
    glVertex3f( ancho, -alto, 0.0f);

    point[0] = inivent.real();
    point[1] = inivent.imag()+tamvent.imag();
    glVertexAttrib1fARB(point_x_location, point[0]);
    glVertexAttrib1fARB(point_y_location, point[1]);
    glColor3f(1.0f,1.0f,1.0f);
    glVertex3f(-ancho, -alto, 0.0f);
    glEnd();


/*
 *
 * */



    if (shadersSupported) {
        glUseProgramObjectARB(0);
    }

    /* swap buffers to display, since we're double buffered. */
    SDL_GL_SwapWindow(window);


    //SDL_FillRect(screen, NULL, color_bg);

    //SDL_Flip(screen);
}

string Gestor::help() {
    return string("\n\t"
            "h: ayuda:\n\t"
            "espacio: sumar uno al display y al step\n\t"
            "p: play/stop\n\t"
            "r: redraw\n\t"
            "i: iteraciones, introducir nuevo valor como criterio de convergencia\n\t"
            "-: restar una iteracion\n\t"
            "+: sumar una iteracion\n\t"
            "w: navegar arriba\n\t"
            "a: navegar izquierda\n\t"
            "s: navegar abajo\n\t"
            "d: navegar derecha\n\t"
            "q: quitar zoom\n\t"
            "e: entrar, aumentar zoom\n\t"
            "c: cuadros por segundo, FPS\n\t"
            "m: modo, deprecated\n\t"
            "t: termino c en la expresion, introducir nuevo valor\n\t"
            "f: nuevo fichero de shader de fragmento\n\t"
//            "v: nuevo fichero de shader de vertice\n\t"
            );
}

void Gestor::onKeyPressed(SDL_Event &e)
{
    int entero;

    if (e.type == SDL_KEYUP)
        return;

    switch(e.key.keysym.sym)
    {
        case SDLK_h:
            cout << help() << endl;
            break;
        case SDLK_SPACE:
            semaforoDisplay.sumar();
            semaforoStep.sumar();
            break;
        case SDLK_p:	// play / stop
            if (semaforoDisplay.estado())
            {
                semaforoStep.cerrar();
                semaforoDisplay.cerrar();
                semaforoDisplay.sumar();
            }
            else
            {
                semaforoStep.abrir();
                semaforoDisplay.abrir();
            }
            break;
        case SDLK_r:	// redraw
            semaforoDisplay.sumar();
            break;
        case SDLK_i:
            cout << "maxiters = " << maxiters << ", nuevo: " << endl;
            SDLcin(maxiters);
            if (maxiters > 5000 || maxiters < 1)
            {
                maxiters = 30;
                cout << "no puedo permitirtelo, amigo" << endl;
            }
            cout << "maxiters = " << maxiters << endl;
            semaforoStep.sumar();
            semaforoDisplay.sumar();
            break;
        case SDLK_KP_MINUS:
            //if (maxiters > 1)
            maxiters--;
            //cout << "maxiters = " << maxiters << endl;
            semaforoStep.sumar();
            semaforoDisplay.sumar();
            break;
        case SDLK_KP_PLUS:
            maxiters++;
            //cout << "maxiters = " << maxiters << endl;
            semaforoStep.sumar();
            semaforoDisplay.sumar();
            break;
        case SDLK_w:
            inivent.imag(inivent.imag() - 0.1*tamvent.imag());
            break;
        case SDLK_s:
            inivent.imag(inivent.imag() + 0.1*tamvent.imag());
            break;
        case SDLK_d:
            inivent.real(inivent.real() + 0.1*tamvent.real());
            break;
        case SDLK_a:
            inivent.real(inivent.real() - 0.1*tamvent.real());
            break;
        case SDLK_e:
            tamvent = tamvent * 0.8;
            inivent.real(inivent.real() + 0.125*tamvent.real());
            inivent.imag(inivent.imag() + 0.125*tamvent.imag());
            break;
        case SDLK_q:
            inivent.real(inivent.real() - 0.125*tamvent.real());
            inivent.imag(inivent.imag() - 0.125*tamvent.imag());
            tamvent = tamvent * 1.25;
            break;
            //case SDLK_f:
            //cout << "Funcion activa actual: " << func_act << ", nuevo: " << endl;
            //SDLcin(func_act);
            //setFuncionActiva(func_act);
            //break;
        case SDLK_c:
            cout << "Nuevos FPS: " << endl;
            SDLcin(entero);
            setFps(entero);
            break;
        case SDLK_m:
            modo = !modo;
            cout << "Cambiando de modo de seleccion de c a : " << modo << endl;
            break;
        case SDLK_t:
        {
            float aux;
            cout << "nueva posicion del termino c: (real(), imag()): (" << endl;
            cin >> aux;
            c.real(aux);
            cout << ",\n";
            cin >> aux;
            c.imag(aux);
            cout << ")\n";
            pos = c;
            break;
        }
        case SDLK_f:
        {
            string shaderPath;
            cin >> shaderPath;
            loadShaders("./jmmut/plotter/shaders/mandelbrot.vs.glsl", (string("./jmmut/plotter/shaders/") + shaderPath).c_str());
            break;
        }
        default:
            break;
    }
    semaforoDisplay.sumar();
}

void Gestor::onMouseMotion(SDL_Event &e)
{
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    complex<double> escala(tamvent.real()*tamvent.real(), -tamvent.imag()*tamvent.imag());

    //cout << "escala = " << escala << endl;	// DEPURACION
    //if (modo)
    pos = pos + complex<double>((e.button.x-xant)*escala.real()/w, (e.button.y-yant)*escala.imag()/h)*0.2;
    //else
    //pos = complex<double>(tamvent.real()*(w/2.0 + float(e.button.x-xant)/w), tamvent.imag()*(h/2.0 + float(e.button.y-yant)/h));

    //cout << "(e.button.x-xant) = " << (e.button.x-xant) << endl;	// DEPURACION
    //cout << "(e.button.y-yant) = " << (e.button.y-yant) << endl;	// DEPURACION
    xant = e.button.x;
    yant = e.button.y;

    c = pos;

    //cout << "pos = " << pos << endl;	// DEPURACION

    semaforoDisplay.cerrar();
    semaforoDisplay.sumar();

}



