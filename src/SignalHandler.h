
#ifndef __SIGNALHANDLER_H_
#define __SIGNALHANDLER_H_

// in case the user doesn't have execinfo.h, or unix's signals: disable backtracing on segfaults:
//#define DO_NOT_BACKTRACE


#ifndef DO_NOT_BACKTRACE
#include <signal.h>
#include <errno.h>
#include <execinfo.h>
#include <sys/types.h>
#include <unistd.h>
#include <cxxabi.h>
#endif // DO_NOT_BACKTRACE

#include <iostream>
#include <stdexcept>

//using std::runtime_error;

class SignalException : public std::runtime_error {
public:
   SignalException(const std::string& _message)
      : std::runtime_error(_message)
   {}
};
/*
class SignalHandler
{
protected:
    static bool mbGotExitSignal;

public:
    SignalHandler();
    ~SignalHandler();

    static bool gotExitSignal();
    static void setExitSignal(bool _bExitSignal);

    void        setupSignalHandlers();
    static void exitSignalHandler(int _ignored);

};*/

/**
 Prints the call stack with some debugging info.
 To use put this somewhere in your program:
 
 SigsegvPrinter::activate(cout);
 */
class SigsegvPrinter {
public:
    static void activate(std::ostream& output_stream);
private:
    static std::ostream *out;
    static void handleSignal(int ignored);
};
//static inline

void print_stacktrace(std::ostream &out, unsigned int max_frames = 63);


//void ActivateSigsegvPrinter(std::ostream & os);
//void handleSignal();

#endif
          